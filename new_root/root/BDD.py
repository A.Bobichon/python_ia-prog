from GLOBAL_VAR import GLOBAL, connected, close


print(GLOBAL())
try:
    g = GLOBAL()
    
    #Create database
    connected(g)
    g.mydb.cursor().execute("CREATE DATABASE IF NOT EXISTS IA;")
    close(g)

    #Create table
    g.set_database("IA")
    connected(g)
    g.mydb.cursor().execute("CREATE TABLE IF NOT EXISTS Charac( id INT PRIMARY KEY NOT NULL AUTO_INCREMENT, niveaux INT, vie INT, attaque INT, def INT);")
    close(g)
    print("the BDD is created!")

except ValueError:
    print(ValueError)