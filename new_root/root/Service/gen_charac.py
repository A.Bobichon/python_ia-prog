from Class.Charac import charac 
import random

charac_pseudo = [
        'a','e','i','y','O','u','ba',
        'ca','da','fa','ga','ha','ja',
        'ka','la','ma','na','pa','qa',
        'ra','sa','ta','va','va','wa',
        'xa','za','be','ce','de','fe',
        'ge','he','je','ke','le','me',
        'ne','pe','qe','re','se','ve',
        'te','ve','we','xe','ze','bi',
        'ci','di','fi','gi','hi','ji',
        'ki','li','mi','ni','pi','qi',
        'ri','si','ti','vi','wi','xi',
        'zi','bo','co','do','fo','go',
        'ho','jo','ko','lo','mo','no',
        'po','qo','ro','so','to','vo',
        'wo','xo','zo','bu','cu','du',
        'fu','gu','hu','ju','ku','lu',
        'mu','nu','pu','qu','ru','su',
        'tu','vu','wu','xu','zu',
    ]

def create_character():
    
    charac_gen = charac()
    pseudo = ''
    rand = lambda x, y : random.randint(x, y)

    niv = rand(0, 10)
    charac_gen.set_niveau(niv)
    charac.set_attaque(rand * niv)
    charac.set_defence(rand * niv)
    charac.set_vie(rand * niv)


    for x in range(0,random.randint(3,8)):
        pseudo += random.choice(charac_pseudo)

    return charac_gen

