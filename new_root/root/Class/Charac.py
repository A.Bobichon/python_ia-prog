
class charac():
    def __init__(self, niveau, vie, attaque, defence):
        self.niveaux = niveau,
        self.vie = vie,
        self.attaque = attaque,
        self.defence = defence

    def set_niveau(self, niveau):
        self.niveau = niveau
        return self

    def get_niveau(self):
        return self.niveau

    def set_vie(self, vie):
        self.vie = vie
        return self

    def get_vie(self):
        return self.vie

    def get_attaque(self):
        return self.attaque

    def set_attaque(self, attaque):
        self.attaque = attaque
        return self

    def get_defence(self):
        return self.defence

    def set_defence(self, defence):
        self.defence = defence
        return self